<?php
namespace addons\sdcmenu\controller\api;

use app\common\controller\Api as ControllerApi;

// 接口基类
class Api extends ControllerApi
{
    const AddonName = 'sdcmenu';
}